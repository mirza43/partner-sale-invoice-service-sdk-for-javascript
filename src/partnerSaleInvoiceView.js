/**
 * The most detailed view of a partner sale invoice
 */
export default class PartnerSaleInvoiceView {

    _id:string;

    _number:string;

    _fileUrl:string;

    _partnerSaleRegistrationId:number;

    /**
     * @param {string} id
     * @param {string} number
     * @param {string} fileUrl
     * @param {(string|null)} partnerSaleRegistrationId
     */
    constructor(
        id:string,
        number:string,
        fileUrl:string,
        partnerSaleRegistrationId:number = null
    ){

        if(!id){
            this._id = new TypeError('id required');
        }
        this._id = id;

        if(!number){
            this._number = new TypeError('number required');
        }
        this._number = number;

        if(!fileUrl){
            this._fileUrl = new TypeError('fileUrl required');
        }
        this._fileUrl = fileUrl;

        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get number():string {
        return this._number;
    }

    /**
     * @returns {string}
     */
    get fileUrl():string {
        return this._fileUrl;
    }

    /**
     * @returns {number|null}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }
}
