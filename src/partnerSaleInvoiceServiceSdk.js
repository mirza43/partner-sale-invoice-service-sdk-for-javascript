import PartnerSaleInvoiceServiceSdkConfig from './partnerSaleInvoiceServiceSdkConfig';
import DiContainer from './diContainer';
import AddPartnerSaleInvoiceReq from './addPartnerSaleInvoiceReq';
import AddPartnerSaleInvoiceFeature from './addPartnerSaleInvoiceFeature';
import GetPartnerSaleInvoiceWithIdFeature from './getPartnerSaleInvoiceWithIdFeature';
import PartnerSaleInvoiceView from './partnerSaleInvoiceView';
import AddPartnerSaleRegIdToPartnerSaleInvoiceFeature from './addPartnerSaleRegIdToPartnerSaleInvoiceFeature';


/**
 * @class {PartnerSaleInvoiceServiceSdk}
 */
export default class PartnerSaleInvoiceServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerSaleInvoiceServiceSdkConfig} config
     */
    constructor(config:PartnerSaleInvoiceServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addPartnerSaleInvoice(request:AddPartnerSaleInvoiceReq,
                          accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddPartnerSaleInvoiceFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerSaleInvoiceWithId(id:string,
                                accessToken:string):Promise<PartnerSaleInvoiceView> {

        return this
            ._diContainer
            .get(GetPartnerSaleInvoiceWithIdFeature)
            .execute(
                id,
                accessToken
            );
    }

    addPartnerSaleRegIdToPartnerSaleInvoice(id:string,
                                            partnerSaleRegistrationId:number,
                                            accessToken:string){

        return this
            ._diContainer
            .get(AddPartnerSaleRegIdToPartnerSaleInvoiceFeature)
            .execute(
                id,
                partnerSaleRegistrationId,
                accessToken
            )

    }

}
