import {inject} from 'aurelia-dependency-injection';
import PartnerSaleInvoiceServiceSdkConfig from './partnerSaleInvoiceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AddPartnerSaleInvoiceReq from './addPartnerSaleInvoiceReq';

@inject(PartnerSaleInvoiceServiceSdkConfig, HttpClient)
class AddPartnerSaleInvoiceFeature {

    _config:PartnerSaleInvoiceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleInvoiceServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Adds a partner sale invoice.
     * @param {AddPartnerSaleInvoiceReq} request
     * @param accessToken
     * @returns {Promise.<string>} id
     */
    execute(request:AddPartnerSaleInvoiceReq,
            accessToken:string):Promise<number> {

        // construct form data
        var formData = new FormData();
        formData.append("number", request.number);
        formData.append("file", request.file);
        if (request.partnerSaleRegistrationId) {
            formData.append("partnerSaleRegistrationId", request.partnerSaleRegistrationId);
        }

        return this._httpClient
            .createRequest('partner-sale-invoices')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(formData)
            .send()
            .then(response => response.content);
    }
}

export default AddPartnerSaleInvoiceFeature;