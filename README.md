## Description
Precor Connect partner sale invoice service SDK for javascript.

## Features

##### Add Partner Sale Invoice
* [documentation](features/AddPartnerSaleInvoice.feature)

##### Get Partner Sale Invoice With Id
* [documentation](features/GetPartnerSaleInvoiceWithId.feature)
## Setup

**install via jspm**  
```shell
jspm install partner-sale-invoice-service-sdk=bitbucket:precorconnect/partner-sale-invoice-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import PartnerSaleInvoiceServiceSdk,{PartnerSaleInvoiceServiceSdkConfig} from 'partner-sale-invoice-service-sdk'

const partnerSaleInvoiceServiceSdkConfig = 
    new PartnerSaleInvoiceServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const partnerSaleInvoiceServiceSdk = 
    new PartnerSaleInvoiceServiceSdk(
        partnerSaleInvoiceServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```