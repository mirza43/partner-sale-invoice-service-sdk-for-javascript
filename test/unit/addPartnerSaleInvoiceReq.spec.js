import AddPartnerSaleInvoiceReq from '../../src/addPartnerSaleInvoiceReq';
import dummy from '../dummy';

/*
 test methods
 */
describe('AddPartnerSaleInvoiceReq class', () => {
    describe('constructor', () => {
        it('throws if number is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleInvoiceReq(
                        null,
                        dummy.file,
                        dummy.partnerSaleRegistrationId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'number required');

        });
        it('sets number', () => {
            /*
             arrange
             */
            const expectedNumber = dummy.partnerSaleInvoiceNumber;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleInvoiceReq(
                    expectedNumber,
                    dummy.file,
                    dummy.partnerSaleRegistrationId
                );

            /*
             assert
             */
            const actualNumber =
                objectUnderTest.number;

            expect(actualNumber).toEqual(expectedNumber);

        });
        it('throws if file is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleInvoiceReq(
                        dummy.partnerSaleInvoiceNumber,
                        null,
                        dummy.partnerSaleRegistrationId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'file required');

        });
        it('sets file', () => {
            /*
             arrange
             */
            const expectedFile = dummy.file;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleInvoiceReq(
                    dummy.partnerSaleInvoiceNumber,
                    expectedFile,
                    dummy.partnerSaleRegistrationId
                );

            /*
             assert
             */
            const actualFile =
                objectUnderTest.file;

            expect(actualFile).toEqual(expectedFile);

        });

        it('does not throw if partnerSaleRegistrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleInvoiceReq(
                        dummy.partnerSaleInvoiceNumber,
                        dummy.file,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();

        });
        it('sets partnerSaleRegistrationId', () => {
            /*
             arrange
             */
            const expectedPartnerSaleRegistrationId = dummy.partnerSaleRegistrationId;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleInvoiceReq(
                    dummy.partnerSaleInvoiceNumber,
                    dummy.file,
                    expectedPartnerSaleRegistrationId
                );

            /*
             assert
             */
            const actualPartnerSaleRegistrationId =
                objectUnderTest.partnerSaleRegistrationId;

            expect(actualPartnerSaleRegistrationId).toEqual(expectedPartnerSaleRegistrationId);

        });
    });
});
