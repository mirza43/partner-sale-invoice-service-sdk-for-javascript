import PartnerSaleInvoiceServiceSdk,
{
    AddPartnerSaleInvoiceReq
} from '../../src/index';
import PartnerSaleInvoiceView from '../../src/partnerSaleInvoiceView';
import factory from './factory';
import config from './config';
import dummy from '../dummy';

/*
 tests
 */
describe('Index module', () => {


    describe('default export', () => {
        it('should be PartnerSaleInvoiceServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new PartnerSaleInvoiceServiceSdk(
                    config.partnerSaleInvoiceServiceSdkConfig
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PartnerSaleInvoiceServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addPartnerSaleInvoice method', () => {
            it('should return partnerSaleInvoiceId', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleInvoiceServiceSdk(
                        config.partnerSaleInvoiceServiceSdkConfig
                    );

                /*
                 act
                 */
                const partnerSaleInvoiceIdPromise =
                    objectUnderTest
                        .addPartnerSaleInvoice(
                            dummy.addPartnerSaleInvoiceReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                partnerSaleInvoiceIdPromise
                    .then((partnerSaleInvoiceId) => {
                        expect(partnerSaleInvoiceId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });
        describe('getPartnerSaleInvoiceWithId method', () => {
            it('should return a PartnerSaleInvoiceView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleInvoiceServiceSdk(
                        config.partnerSaleInvoiceServiceSdkConfig
                    );

                let idOfSeededPartnerSaleInvoice;
                let actualPartnerSaleInvoiceView;

                // seed a partner sale invoice
                const arrangePromise =
                    objectUnderTest
                        .addPartnerSaleInvoice(
                            dummy.addPartnerSaleInvoiceReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(partnerSaleInvoiceId => {
                            idOfSeededPartnerSaleInvoice = partnerSaleInvoiceId
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(() =>
                            objectUnderTest
                                .getPartnerSaleInvoiceWithId(
                                    idOfSeededPartnerSaleInvoice,
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                )
                        )
                        .then(partnerSaleInvoiceView => {
                            actualPartnerSaleInvoiceView = partnerSaleInvoiceView;
                        });

                /*
                 assert
                 */

                actPromise
                    .then(() => {
                        expect(actualPartnerSaleInvoiceView)
                            .toEqual(jasmine.any(PartnerSaleInvoiceView));
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });
        describe('addPartnerSaleRegIdToPartnerSaleInvoice method', () => {
            it('should add a partner sale registration id to a partner sale invoice ', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleInvoiceServiceSdk(
                        config.partnerSaleInvoiceServiceSdkConfig
                    );

                const partnerSaleInvoiceIdPromise =
                    objectUnderTest
                        .addPartnerSaleInvoice(
                            dummy.addPartnerSaleInvoiceReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 act
                 */
                const partnerSaleInvoicePromise =
                    partnerSaleInvoiceIdPromise.then(
                        (partnerSaleInvoiceId) => {
                            objectUnderTest
                                .addPartnerSaleRegIdToPartnerSaleInvoice(
                                    partnerSaleInvoiceId,
                                    dummy.partnerSaleRegistrationId,
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                );
                        }
                    )


                /*
                 assert
                 */
                partnerSaleInvoicePromise
                    .then(() => {
                        //expect().toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });
    });
});