import AddPartnerSaleInvoiceReq from '../src/addPartnerSaleInvoiceReq';

const dummy = {
    firstName: 'firstName',
    lastName: 'lastName',
    accountId: '000000000000000000',
    sapVendorNumber: '0000000000',
    partnerSaleInvoiceId: 'partnerSaleInvoiceId',
    partnerSaleInvoiceNumber: 'partnerSaleInvoiceNumber',
    partnerSaleRegistrationId: 1,
    file: new Blob([3], {type: 'image/png'}),
    userId: 'email@test.com',
    url: 'https://dummy-url.com'
};

dummy.addPartnerSaleInvoiceReq = new AddPartnerSaleInvoiceReq(
    dummy.partnerSaleInvoiceNumber,
    dummy.file,
    dummy.partnerSaleRegistrationId
);

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;